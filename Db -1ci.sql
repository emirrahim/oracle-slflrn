

create table mehsul_cedveli (
    M_ID                         number,
    M_Ad                         varchar2(10),
    M_Qiymet                     number,
    M_Tip                        varchar2(10)
);

insert into mehsul_cedveli 
values(101 , 'Komputer', 1000 , 'Yerli');

insert into mehsul_cedveli 
values(102 , 'Telefon', NULL , 'Xarici');

insert into mehsul_cedveli 
values(103 , 'Saat', 200 , NULL);

insert into mehsul_cedveli 
values(104 , 'Kitab', NULL , 'Yerli');

Alter table mehsul_cedveli 
add M_Istehsalchi varchar2(50);

select * from mehsul_cedveli;


alter table mehsul_cedveli
rename column M_ad to Mehsulun_Adi;

alter table mehsul_cedveli
Modify M_Tip varchar2(25);

alter table mehsul_cedveli
drop column M_Istehsalchi;

rename mehsul_cedveli
to Product;


-- truncate table Product;

UPDATE Product
SET M_Qiymet = 400
Where M_ID = 103;


DELETE from Product
Where M_ID = 104;


select * from Product;

DROP table Product;
-- Product table-ni silib

SELECT * FROM RECYCLEBIN;
-- recyclebin-den datalara baxib

Flashback table Product to before drop;
-- sildiyimiz table-i geri qaytarmaq
